package mams.web;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.HttpResponseStatus;

public class ResponseObject {
	public String type="text/html";
	public String location="";
	public HttpResponseStatus status=HttpResponseStatus.OK;
	public ByteBuf content=Unpooled.wrappedBuffer(new byte[0]);
	public int length=0;
}
