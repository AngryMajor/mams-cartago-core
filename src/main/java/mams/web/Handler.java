package mams.web;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.web.Path;

public interface Handler {
    void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception;
}