package mams.artifacts;

import mams.handlers.BaseHandler;
import mams.web.Handler;
import mams.web.WebServer;
import cartago.*;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class BaseArtifact extends Artifact {
    private BaseHandler base;
    private String uri;
	private String name;
	
	@OPERATION
	void init(String name) {
		System.out.println("Creating base artifact: " + name);
		this.name = name;
	}

    @LINK
	public void attach(String id, Handler handler, OpFeedbackParam<String> baseUri) {
        baseUri.set(this.uri);
        base.addRoute(id, handler);
	}
    
	@LINK
	public void detach(String id){
		base.deleteRoute(id);
	}

    @OPERATION
	void createRoute() {
        String context = "/" + name;
        uri = WebServer.getInstance().getBaseUrl() + context;
		System.out.println("Exposing Agent @ Base Uri: " + uri);
        
        base = new BaseHandler(context);
		try {
			execLinkedOp("out-1", "attach", context, base);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
}
