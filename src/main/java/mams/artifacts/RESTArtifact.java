package mams.artifacts;

import mams.web.WebUtils;
import mams.web.RequestObject;
import mams.web.WebResponse;
import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class RESTArtifact extends Artifact {
    @OPERATION
    void postRequest(String uri, String body, OpFeedbackParam<Integer> code, OpFeedbackParam<String> content) {
        RequestObject requestObject = new RequestObject();
        requestObject.method="POST";
        requestObject.url=uri;
        requestObject.content=body;
        requestObject.type="application/json";
        WebResponse response = WebUtils.sendRequest(requestObject);
        code.set(response.getCode());
        content.set(response.getContent());
    }

    @OPERATION
    void getRequest(String uri, OpFeedbackParam<Integer> code, OpFeedbackParam<String> content) {
        RequestObject requestObject = new RequestObject();
        requestObject.method="GET";
        requestObject.url=uri;
        requestObject.content=null;
        requestObject.type="application/json";
        WebResponse response = WebUtils.sendRequest(requestObject);
        code.set(response.getCode());
        content.set(response.getContent());
    }

    @OPERATION
    void putRequest(String uri, String body, OpFeedbackParam<Integer> code, OpFeedbackParam<String> content) {
        RequestObject requestObject = new RequestObject();
        requestObject.method="PUT";
        requestObject.url=uri;
        requestObject.content=body;
        requestObject.type="application/json";
        WebResponse response = WebUtils.sendRequest(requestObject);
        code.set(response.getCode());
        content.set(response.getContent());
    }

}