package mams.artifacts;

import mams.handlers.ResourceHandler;
import mams.web.Handler;
import cartago.*;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public abstract class ResourceArtifact extends Artifact {
    protected ResourceHandler handler;
    private String uri;
    protected String name;

    public String getUri() {
        return uri;
	}
	
	public ArtifactId getId() {
		return super.getId();
	}
	
	@OPERATION
	void getUri(OpFeedbackParam<String> uri) {
		uri.set(this.uri);
	}
	
    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        baseUri.set(this.uri);
        handler.addRoute(id, childHandler);
	}
	
	@LINK
	public void detach(String id){
		handler.deleteRoute(id);
	}

	@OPERATION
    void update(String name, Object value) {
        this.updateObsProperty(name, value);
	}
	
    @OPERATION
	void createRoute() {
        handler = createHandler();
		try {
            OpFeedbackParam<String> baseUri = new OpFeedbackParam<>();
			execLinkedOp("out-1", "attach", name, handler, baseUri);
            this.uri=baseUri.get() +"/"+ name;
		} catch (OperationException e) {
			e.printStackTrace();
		}
        
	}
	
	public abstract boolean handle(ChannelHandlerContext ctx, FullHttpRequest request);

	public Object getProperty(String name) {
		return getObsProperty(name).getValue();
	}

	protected ResourceHandler createHandler(){
		return new ResourceHandler(this);
	}

}
