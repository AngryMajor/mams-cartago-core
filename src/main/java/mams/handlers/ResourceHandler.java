package mams.handlers;

import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.web.Path;
import mams.web.Handler;
import mams.web.WebServer;


import mams.artifacts.ResourceArtifact;

public class ResourceHandler implements Handler {
    private Map<String, Handler> routes = new TreeMap<String, Handler>();
    private ResourceArtifact artifact;
    public Path path;
    
    
    public ResourceHandler(ResourceArtifact a){
        this.artifact = a;
    }
    
    public void addRoute(String id, Handler handler){
        routes.put(id, handler);
    }
    
    public void deleteRoute(String id){
        routes.remove(id);
    }

    protected ResourceArtifact getArtifact() {
        return artifact;
    }

    public ResourceArtifact[] getChildren() {
        ResourceArtifact[] array = new ResourceArtifact[routes.size()];
        int index = 0;
        for (Handler handler : routes.values()) {
            array[index++] = ((ResourceHandler) handler).getArtifact();
        }
        return array;
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {
        this.path = path;
        if (this.path.length() > 0) {
            Handler handler = routes.get(this.path.prefix());
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else {
            // System.out.println("Method: " + request.method().toString());
            // System.out.println("Uri: " + request.uri().toString());
            artifact.handle(ctx, request);
        }
	}

    public Set<String> getLinks(){
        return routes.keySet();
    }

}

