package mams.utils;

import cartago.util.agent.*;
import cartago.CartagoService;

public class CartagoBackend {
    private static CartagoBasicContext ctx;
    
    public static CartagoBasicContext getInstance() {
        if (ctx == null) {
            ctx = new CartagoBasicContext("_backend", CartagoService.MAIN_WSP_NAME);
        }
        return ctx;
    }
}